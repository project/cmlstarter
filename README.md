# _cml_starter_

## Чем занимается?

> Добавляет поля, таксономии, представления, шаблоны путей, стили изображений, блоки, сущности модуля commerce для реализации базового функционала интернет-магазина.

## Требования к платформе

- _Drupal 8-10_
- _PHP 7.4.0+_

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/cmlstarter)

```sh
composer require 'drupal/cmlstarter:^1.82'
```

- [Drupal.org dev версия](https://www.drupal.org/project/cmlstarter/releases/8.x-1.x-dev)

```sh
composer require 'drupal/cmlstarter:1.x-dev@dev'
```

## Как использовать?
